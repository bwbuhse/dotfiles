# Choose completers
zstyle ':completion:*' completer _extensions _complete _approximate

# Enable completion cache
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$HOME/.cache/zsh/.zcompcache"

# Enable completion menu
zstyle ':completion:*' menu selectzstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
# Format the completion menu
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
# Group completion results by description
zstyle ':completion:*' group-name ''

# When true expand // to /
# When false expand // to /*/
zstyle ':completion:*' squeeze-slashes true

# Set matchers?
zstyle ':completion:*' matcher-list '' 'r:|[._-]=** r:|=**' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'l:|=* r:|=*'
# zstyle :compinstall filename '/Users/benjaminb/.zshrc'
