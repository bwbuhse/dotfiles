#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#   Ben Buhse <me@benbuhse.com>

# Enable vi mode
bindkey -v

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Source Virtualenvwrapper
if [[ -s "/usr/share/virtualenvwrapper/virtualenvwrapper.sh" ]]; then
  source "/usr/share/virtualenvwrapper/virtualenvwrapper.sh"
fi

# Add nimble binaries (nim's package manager) to path if they exist
if [[ -s "/home/ben/.nimble/bin/" ]]; then
  PATH+=":/home/ben/.nimble/bin/"
fi

# Customize to your needs...
# Check for other .zsh files then source if they exist
# General files
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
test -e "${HOME}/.completions.zsh" && source "${HOME}/.completions.zsh"

# Alias files
test -e "${HOME}/.aliases.zsh" && source "${HOME}/.aliases.zsh"
test -e "${HOME}/.lab_aliases.zsh" && source "${HOME}/.lab_aliases.zsh"
test -e "${HOME}/.work_aliases.zsh" && source "${HOME}/.work_aliases.zsh"

# Function files
test -e "${HOME}/.functions.zsh" && source "${HOME}/.functions.zsh"
test -e "${HOME}/.lab_functions.zsh" && source "${HOME}/.lab_functions.zsh"

