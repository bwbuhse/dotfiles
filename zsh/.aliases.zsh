# Run neovim instead of vim when nvim is installed
if (( $+commands[nvim] )); then
  alias vim='nvim'
fi

# Run bat instead of cat when bat is installed
if (( $+commands[bat] )); then
  alias cat='bat'
elif (( $+commands[batcat] )); then
  # Needed for Ubuntu
  alias cat='batcat'
fi

# Run exa instead of ls when exa is installed
# On top of that, set ll and la
if (( $+commands[exa] )); then
  alias ls='exa'
  alias ll='exa -l'
  alias la='exa -la'
else
  alias ll='ls -l --color=auto'
  alias la='ls -la --color=auto'
fi

# Run fd instead of find when fd is installed
if (( $+commands[fd] )); then
  alias find='fd'
elif (( $+commands[fdfind] )); then
  # Needed for Ubuntu
  alias fd='fdfind'
fi

# Various git aliases
alias gl='git log'
alias gc='git commit'
alias gcm='git commit -m'
alias gws='git status'
alias gd='git diff'
alias gs='git stash'
alias gsp='git stash pop'
alias gp='git push'
alias gpl='git pull'
alias ga='git add'
alias gfu='echo "git fetch upstream; git checkout master; git merge upstream/master"'
