---------------------------------------
-- My Dracula-inspired awesome theme --
---------------------------------------

local theme_dir = require("gears.filesystem").get_configuration_dir() .. "themes/dracula/"
local dpi = require("beautiful.xresources").apply_dpi

-- {{ Main variable
local theme = {}
-- }}

-- {{ Styles

-- {{ Font
theme.font = "Cascadia Code PL 10"
-- }}

-- [[
-- Import my Dracula Pro wallpaper and colors.
-- If the file isn't there, use equivalent FOSS Dracula options.
-- Anyone who wants to copy this theme without the Dracula Pro options
-- just needs to remove the pcall and the <true> branch of the if else.
-- ]]
local status_ok, dracula_pro = pcall(require, "themes.dracula.dracula_pro")
if status_ok then
  -- {{{ Colors
  theme.bg_normal   = dracula_pro.bg_normal
  theme.bg_focus    = dracula_pro.bg_focus
  theme.bg_urgent   = dracula_pro.bg_urgent
  theme.bg_minimize = dracula_pro.bg_minimize
  theme.bg_systray  = dracula_pro.bg_systray

  theme.fg_normal   = dracula_pro.fg_normal
  theme.fg_focus    = dracula_pro.fg_focus
  theme.fg_urgent   = dracula_pro.fg_urgent
  theme.fg_minimize = dracula_pro.fg_minimize

  theme.border_normal = dracula_pro.border_normal
  theme.border_focus  = dracula_pro.border_focus
  theme.border_marked = dracula_pro.border_marked
  -- }}}

  -- {{{ Wallpaper
  theme.wallpaper = theme_dir .. "dracula-pro.png"
  -- }}}
else
  theme.bg_normal   = "#222222"
  theme.bg_focus    = "#535d6c"
  theme.bg_urgent   = "#ff0000"
  theme.bg_minimize = "#444444"
  theme.bg_systray  = theme.bg_normal

  theme.fg_normal   = "#aaaaaa"
  theme.fg_focus    = "#ffffff"
  theme.fg_urgent   = "#ffffff"
  theme.fg_minimize = "#ffffff"

  theme.border_normal = "#282A36"
  theme.border_focus  = "#6272A4"
  theme.border_marked = "#44475A"

  theme.wallpaper = theme_dir .. "dracula.png"
end

-- {{{ Taglist
theme.taglist_squares_sel   = theme_dir .. "taglist/squarefz.png"
theme.taglist_squares_unsel = theme_dir .. "taglist/squarez.png"
-- }}}

-- {{{ Borders
theme.useless_gap   = dpi(4)
theme.border_width  = dpi(3)
-- }}}


-- }}

return theme
