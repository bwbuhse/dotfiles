set -g VIRTUALFISH_VERSION 2.5.4
set -g VIRTUALFISH_PYTHON_EXEC /usr/bin/python
# Make sure it exists
if test -e "$HOME/.local/lib/python3.8/site-packages/virtualfish"
    source /home/benjaminb/.local/lib/python3.8/site-packages/virtualfish/virtual.fish
    source /home/benjaminb/.local/lib/python3.8/site-packages/virtualfish/compat_aliases.fish
    source /home/benjaminb/.local/lib/python3.8/site-packages/virtualfish/environment.fish
    source /home/benjaminb/.local/lib/python3.8/site-packages/virtualfish/auto_activation.fish
end
emit virtualfish_did_setup_plugins
