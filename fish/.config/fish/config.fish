if status is-interactive
    # Commands to run in interactive sessions can go here
end

# Add $HOME/.local/bin to PATH if it exists
if test -e "$HOME/.local/bin"
    fish_add_path $HOME/.local/bin
end

# Add nimble's (nim lang's package manager) installation dir to PATH if it exists
if test -e "$HOME/.nimble/bin"
    fish_add_path $HOME/.nimble/bin
end

# Set up pyenv
if test -e "/usr/bin/pyenv"
    status is-login; and pyenv init --path | source
    status is-interactive; and pyenv init - | source
end

# Add go to PATH if it exists
if test -e "/usr/local/go/bin"
    fish_add_path /usr/local/go/bin
end
# And add go modules, too (I'm not sure if 'modules' is the word go uses, but whatever)
if test -e "$HOME/go/bin"
    fish_add_path $HOME/go/bin
end

# Add Cargo bin directory to PATH if it exists
if test -e "$HOME/.cargo/bin"
    fish_add_path $HOME/.cargo/bin
end


# Set EDITOR
if test -q $nvim
    set -Ux EDITOR nvim
else if test -q $vim
    set -Ux EDITOR vim
else if test -q $vi
    set -Ux EDITOR vi
end

# Use starship prompt if starship is installed
if test -e "/usr/bin/starship"; or test -e "/usr/local/bin/starship"
    starship init fish | source
end
