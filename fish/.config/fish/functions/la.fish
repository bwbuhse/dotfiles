function la -d 'Run "ls -la" or "exa -la" (if exa is installed)'
  if command -q exa
    command exa -la $argv
  else
    command ls -la --color=auto $argv
  end
end
