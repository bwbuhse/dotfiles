function gfu -d 'Git fetch upstream, checkout master, then merge upstream/master'
  command echo "git fetch upstream; git checkout master; git merge upstream/master"
end
