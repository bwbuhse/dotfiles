function find -d 'Run fd instead of find if fd is installed'
    if command -q fd 
        command fd $argv
    else if command -q fdfind
        command fdfind $argv
    else
        command find $argv
    end
end

function fd -d "Alias fdfind to fd (for Ubuntu)"
    if command -q fd
        command fd $argv
    else if command -q fdfind
        command fdfind $argv
    else 
        echo "fd is not installed on this system!"
    end 
end
