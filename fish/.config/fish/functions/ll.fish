function ll -d 'Run "ls -l" or "exa -l" (if exa is installed)'
  if command -q exa
    command exa -l $argv
  else
    command ls -l --color=auto $argv
  end
end
