function cat -d 'Run bat instead of cat if bat is installed'
    if command -q bat
        command bat $argv
    else if command -q batcat
        command batcat $argv
    else
        command cat $argv
    end
end
