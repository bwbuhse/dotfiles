function gsp -w git -d 'alias gsp="git stash pop"'
  command git stash pop $argv
end
