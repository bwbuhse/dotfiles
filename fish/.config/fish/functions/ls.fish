function ls -d 'Run exa instead of ls if exa is installed'
  if command -q exa
    command exa $argv
  else
    command ls $argv
  end
end
