local wezterm = require 'wezterm';

return {
  leader = { key = "a", mods = "CTRL" },
  keys = {
    -- LEADER-r will put us in resize-pane
    -- mode until we cancel that mode.
    { key = "r", mods = "LEADER", action = wezterm.action {
      ActivateKeyTable = {
        name = "resize_pane",
        one_shot = false,
        replace_current = true,
      }
    } },

    -- LEADER-a will put us in activate-pane
    -- mode until we press some other key or until 1 second (1000ms)
    -- of time elapses
    { key = "a", mods = "LEADER", action = wezterm.action {
      ActivateKeyTable = {
        name = "activate_pane",
        timeout_milliseconds = 1000,
        one_shot = true,
        replace_current = false,
      }
    } },

    -- LEADER-"\" will split vertically
    { key = "\\", mods = "LEADER", action = wezterm.action { SplitHorizontal = { domain = "CurrentPaneDomain" } } },
    -- LEADER-"-" will split horizontally
    { key = "-", mods = "LEADER", action = wezterm.action { SplitVertical = { domain = "CurrentPaneDomain" } } },

    -- LEADER-h activates the pane on the left
    { key = "LeftArrow", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Left" } },
    { key = "h", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Left" } },

    -- LEADER-l activates the pane on the right
    { key = "RightArrow", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Right" } },
    { key = "l", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Right" } },

    -- LEADER-k activates the pane on the top
    { key = "UpArrow", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Up" } },
    { key = "k", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Up" } },

    -- LEADER-j activates the pane on the bottom
    { key = "DownArrow", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Down" } },
    { key = "j", mods = "LEADER", action = wezterm.action { ActivatePaneDirection = "Down" } },

    -- LEADER-d detaches the current domain
    { key = "d", mods = "LEADER", action = wezterm.action.DetachDomain("CurrentPaneDomain") },

    -- Send "CTRL-A" to the terminal when pressing CTRL-A, CTRL-A
    -- We need this because we use CTRL-A as a leader
    { key = "a", mods = "LEADER", action = wezterm.action { SendString = "\x01" } },

    -- Clears the scrollback buffer and leave the viewport intact
    { key = "k", mods = "CTRL", action = wezterm.action { ClearScrollback = "ScrollbackOnly" } },

    -- Close current pane
    { key = "w", mods = "CTRL|SHIFT", action = wezterm.action.CloseCurrentPane { confirm = true } },

  },

  key_tables = {
    -- Defines the keys that are active in our resize-pane mode.
    -- Since we're likely to want to make multiple adjustments,
    -- we made the activation one_shot=false. We therefore need
    -- to define a key assignment for getting out of this mode.
    -- 'resize_pane' here corresponds to the name="resize_pane" in
    -- the key assignments above.
    resize_pane = {
      { key = "LeftArrow", action = wezterm.action { AdjustPaneSize = { "Left", 1 } } },
      { key = "h", action = wezterm.action { AdjustPaneSize = { "Left", 1 } } },

      { key = "RightArrow", action = wezterm.action { AdjustPaneSize = { "Right", 1 } } },
      { key = "l", action = wezterm.action { AdjustPaneSize = { "Right", 1 } } },

      { key = "UpArrow", action = wezterm.action { AdjustPaneSize = { "Up", 1 } } },
      { key = "k", action = wezterm.action { AdjustPaneSize = { "Up", 1 } } },

      { key = "DownArrow", action = wezterm.action { AdjustPaneSize = { "Down", 1 } } },
      { key = "j", action = wezterm.action { AdjustPaneSize = { "Down", 1 } } },

      -- Cancel the mode by pressing escape
      { key = "Escape", action = "PopKeyTable" },

    },

  }
}
