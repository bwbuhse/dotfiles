local wezterm = require 'wezterm'
local keybinds = require 'keybinds'
local hostname = wezterm.hostname()

local font_size
if hostname == 'USAU-BENJAMINB-M' or hostname == 'rd-benjaminb' then
  -- My Mac does weird scaling with the same font size on the same montior *shrug*local
  font_size = 16.0
else
  font_size = 13.0
end

return {
  -- Font Settings
  font = wezterm.font_with_fallback({
    {
      family = "Cascadia Code PL",
      harfbuzz_features = { "calt", "ss01" } -- Enable cursive italics
    },
    {
      family = "Font Awesome 5 Free",
    }
  }),
  font_size = font_size,

  -- Color Scheme
  color_scheme = "Catppuccin Mocha",

  -- Window Settings
  window_background_opacity = 0.95,
  hide_tab_bar_if_only_one_tab = true,
  tab_bar_at_bottom = true,
  use_fancy_tab_bar = false,

  --[===[
    -- Wezterm SSH Daemon
    -- Allows wezterm to connect to remote wezterm sessions
    -- and act as a multiplexer like tmux
    --]===]
  ssh_domains = {
    {
      name = "rd-benjaminb",
      remote_address = "rd-benjaminb",
      username = "benjaminb",
      default_prog = { "fish" },
    },
    {
      name = "hatpad",
      remote_address = "hatpad",
      username = "ben",
    }
  },
  unix_domains = {
    {
      name = "rd-benjaminb-unix",
      proxy_command = { "ssh", "-T", "-A", "benjaminb@rd-benjaminb", "wezterm", "cli", "proxy" },
    },
  },
  mux_env_remove = {
    -- 'SSH_AUTH_SOCK',
    'SSH_CLIENT',
    'SSH_CONNECTION',
  },

  --- Keybinds from keybinds.lua
  leader = keybinds.leader,
  keys = keybinds.keys,
  key_tables = keybinds.key_tables,
  set_environment_variables = {
    SSH_AUTH_SOCK = "/home/benjaminb/.ssh/ssh_auth_sock",
  },
}
