-- Helper functions
local function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

local function nmap(shortcut, command)
  map('n', shortcut, command)
end

local function imap(shortcut, command)
  map('i', shortcut, command)
end

local function vmap(shortcut, command)
  map('v', shortcut, command)
end

-- Make Y yank from cursor to end of the line (instead of the entire line)
nmap("Y", "yg_")

-- Keep cursor centered when scrolling
nmap("n", "nzzzv")
nmap("N", "Nzzzv")
nmap("J", "mzJ`z")

-- Keybinds to help with moving text around
vmap("J", ":m '>+1<CR>gv=gv")
vmap("K", ":m '<-2<CR>gv=gv")

-- Clear highlights with <C-h>
nmap("<C-h>", ":noh<CR>")

-- Telescope.nvim keybinds
nmap("<leader>ff", "<cmd>Telescope find_files<cr>")
nmap("<leader>fg", "<cmd>Telescope live_grep<cr>")
nmap("<leader>fb", "<cmd>Telescope buffers<cr>")
nmap("<leader>fh", "<cmd>Telescope help_tags<cr>")

-- nvim-tree keybinds
nmap("<C-n>", ":NvimTreeToggle<CR>") -- Toggle the file tree
