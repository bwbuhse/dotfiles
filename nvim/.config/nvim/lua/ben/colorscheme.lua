-- Dracula.nvim configuration
-- local dracula = require("dracula")

-- load Dracula Pro colors
-- local pro_colors = require('ben.dracula_pro')

-- dracula.setup({
--   -- Use Dracula Pro colors, rather than FOSS
--   colors = pro_colors,
--   -- use transparent background
--   transparent_bg = true,
--   -- use italic comments in supported fonts
--   italic_comment = true,
-- })

-- catppuccin/nvim configuration
local status_ok, catppuccin = pcall(require, "catppuccin")
if not status_ok then
	return
end

vim.g.catppuccin_flavour = "mocha" -- latte, frappe, macchiato, mocha

catppuccin.setup({ -- use transparent background
	flavour = "mocha",
	transparent_background = true,
	-- use italic comments in supported fonts
	-- but not italic conditionals
	styles = {
		comments = { "italic" },
		conditionals = {},
	},
})

-- Select colorscheme
vim.cmd.colorscheme("catppuccin")
