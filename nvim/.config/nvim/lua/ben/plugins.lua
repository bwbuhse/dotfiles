local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

return packer.startup(function(use)
	-- Packer can manage itself
	use("wbthomason/packer.nvim")

	-- Dracula.nvim (Dracula theme written in Lua)
	-- https://github.com/Mofiqul/dracula.nvim
	-- use 'Mofiqul/dracula.nvim'

	-- vim-commentary: comment stuff in and out easily
	-- https://github.com/tpope/vim-commentary
	use("tpope/vim-commentary")

	-- telescope.nvim: highly extendable fuzzy finder for nvim
	-- https://github.com/nvim-telescope/telescope.nvim
	use({
		"nvim-telescope/telescope.nvim",
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	-- nvim-treesitter: treesitter for nvim
	-- 	I mostly use it for its advanced highlighting
	-- https://github.com/nvim-treesitter/nvim-treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
	})
	use("nvim-treesitter/playground")

	-- shows git diff in the gutter bar
	use("lewis6991/gitsigns.nvim")

	-- nvim-tree: nice file trees
	use({
		"kyazdani42/nvim-tree.lua",
		requires = {
			"kyazdani42/nvim-web-devicons", -- optional, for file icons
		},
		tag = "nightly", -- optional, updated every week. (see issue #1193)
	})

	-- completion plugins
	use("hrsh7th/nvim-cmp") -- adds completions
	use("hrsh7th/cmp-buffer") -- buffers
	use("hrsh7th/cmp-path") -- paths
	use("hrsh7th/cmp-cmdline") -- command line
	use("saadparwaiz1/cmp_luasnip") -- snippets
	use("hrsh7th/cmp-nvim-lsp") -- lsp
	use("hrsh7th/cmp-nvim-lua") -- nvim config lsp

	-- snippets
	use("L3MON4D3/LuaSnip") -- snippet engine
	use("rafamadriz/friendly-snippets") -- a bunch of useful snippets

	-- LSP
	use("neovim/nvim-lspconfig") -- enable LSP
	use("williamboman/mason.nvim") -- simple to use language server installer
	use("williamboman/mason-lspconfig.nvim") -- simple to use language server installer
	use({
		"jose-elias-alvarez/null-ls.nvim", -- LSP diagnostics and code actions
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	-- Catppuccin theme
	use({ "catppuccin/nvim", as = "catppuccin" })

	-- nim support?
	-- honestly, not sure if I need this
	use("zah/nim.vim")

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
