return {
	settings = {
		Lua = {
			diagnostics = {
				globals = {
          -- neovim's API for vim settings
          "vim",
          -- awesomewm's various globals
          "awesome",
          "client",
          "root",
          "screen",
        },
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
		},
	},
}
