-- Miscellanious vim settings
local o = vim.opt
o.number = true
o.relativenumber = true
o.syntax = "on"
o.tabstop = 4
o.softtabstop = 0
o.expandtab = true
o.shiftwidth = 4
o.smarttab = true
o.encoding = "utf-8"
o.hidden = true -- ??
o.cmdheight = 2 -- more space for displaying messages
o.updatetime = 300 -- default is 4000ms
vim.api.nvim_command [[set shortmess+=c]] -- Don't pass messages to |ins-completion-menu|.
o.termguicolors = true
o.signcolumn = "yes"
o.backup = false
o.ignorecase = true -- Ignore case in searches
o.termguicolors = true
o.splitbelow = true -- force all horizontal splits to go below current window
o.splitright = true -- force all vertical splits to go to the right of current window
o.mouse = ""
