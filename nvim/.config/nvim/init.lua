require 'ben.options'
require 'ben.plugins'
require 'ben.keymaps'
require 'ben.cmp'
require 'ben.lsp'
require 'ben.treesitter'
require 'ben.colorscheme'

-- Basic plugin setups
-- Anything more complex gets its own file
require("nvim-tree").setup()
require('gitsigns').setup()
