-- Use tab characters instead of spaces in Golang
local o = vim.opt_local
o.expandtab = false
o.smarttab = false
