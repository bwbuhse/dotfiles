local o = vim.opt_local

-- Set the comment string for elixir (for vim-commentary)
o.commentstring = "# %s"

-- Use 2 spaces instead of 4 for lua
o.shiftwidth = 2
o.tabstop = 2
o.expandtab = true
o.smarttab = true
